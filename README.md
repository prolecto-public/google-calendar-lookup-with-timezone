PRACTICE
Here is the article on the supporting use and practice:
https://blog.prolecto.com/2021/05/22/get-a-google-calendar-lookup-with-timezone/



INSTALLATION
To install this applcation, perform the following:
1.  Navigate to https://script.google.com/   Login under the right google account to host the Web App.
2.  Create a new project.
3.  The default file is Code.gs in a new project.  Add the code from Code.js
4.  Create a new HTML file called 'index'.  It will automatically name it index.html
5.  Deploy the application
    a. New Deployment as Web App. 
    b. Set permissions accordingly.
    c. Test the deployment.  When it is good, use the deployment link under Web App



COMMAND LINE INSTRUCTIONS FOR CORE REPOSITORY
You can also upload existing files from your computer using the instructions below.


Git global setup
git config --global user.name "Marty Zigman"
git config --global user.email "marty.zigman@prolecto.com"

Create a new repository
git clone git@gitlab.com:prolecto-public/google-calendar-lookup-with-timezone.git
cd google-calendar-lookup-with-timezone
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Push an existing folder
cd existing_folder
git init
git remote add origin git@gitlab.com:prolecto-public/google-calendar-lookup-with-timezone.git
git add .
git commit -m "Initial commit"
git push -u origin master

Push an existing Git repository
cd existing_repo
git remote rename origin old-origin
git remote add origin git@gitlab.com:prolecto-public/google-calendar-lookup-with-timezone.git
git push -u origin --all
git push -u origin --tags
