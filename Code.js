function doGet() {
  let template = HtmlService.createTemplateFromFile("index");

  let defaultTz = CalendarApp.getDefaultCalendar().getTimeZone();
  template.defaultTz = defaultTz;

  return template.evaluate();
}

function getEvents(searchKey, timezoneAndOffset) {
  let timezone = timezoneAndOffset.split(" ")[0];

  let defaultTz = CalendarApp.getDefaultCalendar().getTimeZone();

  let cals = CalendarApp.getAllCalendars();
  let twoDaysAgo = new Date(),
    fourWeeksLater = new Date();

  twoDaysAgo.setTime(Date.now() - 2 * 24 * 3600 * 1000);
  fourWeeksLater.setTime(Date.now() + 4 * 7 * 24 * 3600 * 1000);

  const fixOneMinute = (s) => s.replace(":01", ":00");

  function formatTime(e, timezone) {
    let months = [
      "JAN",
      "FEB",
      "MAR",
      "APR",
      "MAY",
      "JUN",
      "JUL",
      "AUG",
      "SEP",
      "OCT",
      "NOV",
      "DEC",
    ];

    let m = months[e.getStartTime().getMonth()];

    let [d, ...rest] = Utilities.formatDate(
      e.getStartTime(),
      timezone,
      "dd yyyy, E"
    )
      .toUpperCase()
      .split(" ");
    return (
      [d, m, ...rest].join(" ") +
      " " +
      fixOneMinute(
        Utilities.formatDate(e.getStartTime(), timezone, "hh:mm")
      ).toLowerCase() +
      " — " +
      fixOneMinute(
        Utilities.formatDate(e.getEndTime(), timezone, "hh:mma").toLowerCase()
      )
    );
  }

  let events = cals

    .map((c) => {
      let color = c.getColor();
      return c
        .getEvents(twoDaysAgo, fourWeeksLater)
        .filter((e) =>
          e.getTitle().toLowerCase().includes(searchKey.toLowerCase())
        )
        .map((e) => [e, color]);
    })
    .flat(1)
    .map(([e, color]) => ({
      title: e.getTitle(),
      color,
      timeInDefaultTz: formatTime(e, defaultTz),
      timeInTargetTz: formatTime(e, timezone),
    }));

  return {
    events,
    defaultTimeZone: defaultTz,
    selectedTimeZone: timezoneAndOffset,
  };
}
